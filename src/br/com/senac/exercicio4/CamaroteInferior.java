
package br.com.senac.exercicio4;


public class CamaroteInferior extends Vip{
    
    private String localizacao;
    
    public CamaroteInferior(double valor , String localizacao) {
        super(valor);
        this.localizacao = localizacao;
    }
    
    public void ImprimirLocalizacao(){
        System.out.println("Localização:" + this.localizacao);
    }
    
    
}
