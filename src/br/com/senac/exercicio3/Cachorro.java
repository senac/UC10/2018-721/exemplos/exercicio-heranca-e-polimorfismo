package br.com.senac.exercicio3;

public class Cachorro extends Animal {

    @Override
    public void caminha() {
        System.out.println("Cachorro caminhando ........");
    }

    public void latir() {
        System.out.println("Au Au !");
    }

    @Override
    public void falar() {
        this.latir();
    }
}
