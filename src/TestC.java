
import br.com.senac.exercicio3.Animal;
import br.com.senac.exercicio3.Cachorro;
import br.com.senac.exercicio3.Miseravel;
import br.com.senac.exercicio3.Pessoa;
import br.com.senac.exercicio3.Pobre;
import br.com.senac.exercicio3.Rica;
import java.util.ArrayList;
import java.util.List;
import br.com.senac.exercicio3.Comunicavel;
import br.com.senac.exercicio3.Gato;

public class TestC {

    public static void main(String[] args) {

        Rica rica = new Rica();
        Pobre pobre = new Pobre();
        Miseravel miseravel = new Miseravel();
        Gato gato = new Gato();
        Cachorro cachorro = new Cachorro();

        List<Pessoa> pessoas = new ArrayList<>();

        List<Comunicavel> lista = new ArrayList<>();

        pessoas.add(pobre);
        pessoas.add(rica);
        pessoas.add(miseravel);

        lista.add(gato);
        lista.add(cachorro);
        lista.add(rica);
        lista.add(pobre);
        lista.add(miseravel);

        for (Comunicavel c : lista) {

            if (c instanceof Animal) {
                Animal a = (Animal) c;
                a.caminha();
            }

        }

    }

}
