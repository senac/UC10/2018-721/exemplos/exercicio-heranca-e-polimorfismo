package br.com.senac.exercicio4;

public class CamaroteSuperior extends Vip {

    private double adicional = 100;

    public CamaroteSuperior(double valor) {
        super(valor);
    }

    public double getValor() {
        return valor + this.adicional;
    }

}
