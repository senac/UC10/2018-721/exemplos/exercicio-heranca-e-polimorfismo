
package br.com.senac.exercicio4;

public class Normal extends Ingresso{
    
    public Normal(double valor) {
        super(valor);
    }
    
    
    public void imprimir(){
        System.out.println("Ingresso normal");
    }
    
    
}
