package br.com.senac.exercicio2;

public class Tecnico extends Assistente {

    private double bonus;

    public Tecnico(String matricula) {
        super(matricula);
    }

    public Tecnico(String matricula, double bonus) {
        super(matricula);
        this.bonus = bonus;
    }

    @Override
    public double getSalario() {

        return super.getSalario() + this.getBonus();

    }

    public double getBonus() {
        return bonus;
    }

}
