package br.com.senac.exercicio5;

import br.com.senac.exercicio4.StringUtils;

public class Novo extends Imovel {

    private final double adicional;

    public Novo(String endereco, double preco, double adicional) {
        super(endereco, preco);
        this.adicional = adicional;
    }

    public void getValorImovel() {
        System.out.println(StringUtils.formatarMonetario(this.getPreco()));
    }

    public double getAdicional() {
        return adicional;
    }

    @Override
    public double getPreco() {
        return this.preco + this.adicional;
    }

}
