package br.com.senac.exercicio1;

public class ContaPoupanca extends Conta {

    public ContaPoupanca() {
        super(0);
    }

    @Override
    public void sacar(double valor) {
       if(this.getSaldo() >= valor ){
            this.saldo -= valor ;
        }else{
            throw new RuntimeException("Saque excede limite") ; 
        }
    }

}
