package br.com.senac.exercicio2;

public class Administrativo extends Assistente {

    
    private String turno ; 
    private final double adicionalNoturno = 0.25; 
    
    
    public Administrativo(String matricula , String turno) {
        super(matricula);
        this.turno = turno;
    }

    @Override
    public double getSalario() {
    
        return  this.turno.equalsIgnoreCase("Nortuno") ? super.getSalario() + (super.getSalario() * adicionalNoturno)  : super.getSalario() ;
        

    }
    
    
    
    
    
    

}
