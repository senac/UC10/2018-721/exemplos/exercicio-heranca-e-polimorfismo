package br.com.senac.exercicio5;

import br.com.senac.exercicio4.StringUtils;

public class Velho extends Imovel {

    private double desconto;

    public Velho(String endereco, double preco) {
        super(endereco, preco);
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    @Override
    public double getPreco() {
        return this.preco - this.desconto;
    }

    public void getValorImovel() {
        System.out.println(StringUtils.formatarMonetario(this.getPreco()));
    }

}
